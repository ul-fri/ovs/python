# Približek binomske porazdelitve z normalno

Za velike vrednosti $n$ je 

$$B(n,p)\sim N(\mu, \sigma)$$

za $\mu = np$ in $\sigma =\sqrt{np(1-p)}$.



```python
from scipy.stats import binom, norm
n = 10
p = 0.5
X = binom(n, p) # število grbov v 100 metih kovanca
print("P(X < 7) = {}".format(X.cdf(6)))
```

    P(X < 7) = 0.828125



```python
import matplotlib.pyplot as plt
import numpy as np

x = range(n+1)
plt.bar(x, [X.pmf(xi) for xi in x], label="B({}, {})".format(n, p))
mu = n*p 
sigma = (n*p*(1-p))**0.5
Y = norm(mu, sigma)
t = np.arange(0, 10, 0.1)
plt.plot(t, [Y.pdf(xi) for xi in t], color="red", label="N({}, {:.2})".format(mu, sigma))
plt.title("Binomska porazdelitev in normalni približek")
plt.legend()
```




    <matplotlib.legend.Legend at 0x7f065ef06c50>




![png](binomska_normalna_files/binomska_normalna_2_1.png)


## Naloga

Razloži in reproduciraj graf
![porazdelitve vzorčnih deležev](https://img.rtvslo.si/_up/upload/2020/04/22/65670006.jpg)
v [članku](https://www.rtvslo.si/kolumne/zakaj-je-pomembno-da-sprejmemo-vabilo-k-sodelovanju-v-studiji-o-razsirjenosti-koronavirusa/521435) o tem zakaj je pomembno, da je vzorec testiranja čim večji.

Z normalnim približkom za binomsko porazdelitev določi 90% intervale za vse zgornje porazdelitve.


```python

```
