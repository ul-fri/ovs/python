# Verjetnost in statistika v Pythonu

Priporočamo, da si namestite [Python](https://www.python.org) in [Jupyter](https://jupyter.org/install.html) na svoj računalnik. Če nimate te možnosti, lahko uporabite Binder tako, da kliknete na spodnjo ikonico.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ul-fri%2Fovs%2Fpython/master)

## Priprava okolja

Kodo, ki je v tem repozitoriju si najlažje namestite z [git](https://git-scm.com/) (če še ne poznate orodja git, je skrajni čas, da ga spoznate):

```bash
git clone https://gitlab.com/ul-fri/ovs/python vs-python
cd vs-python
```

Z zgornjim ukazom ste si ustvarili lokalno delovno kopijo celotnega repozitorija.
Če ste si namestili jupyter, lahko začnete z delom

```bash
jupyter-lab
```

oziroma

```bash
jupyter-notebook
```

za starejšo verzijo vmesnika. Pojaviti bi se moralo okno brskalnika z Jupyter
vmesnikom.


## Namestitev jupyter

### Lokalna namestitev

Najbolje je, če si jupyter namestite lokalno na svojem računalniku. Pred tem si morate [namestiti Python](https://www.python.org/downloads/). Jupyter ponuja precej dobra [navodila za namestitev](https://jupyter.org/install.html), zato jih ne bi ponavljali na tem mestu.

### Docker

Uporabite lahko tudi [docker](https://www.docker.com/), ki omogoča pripravo samozadostnih kontejnerjev z različnimi programi. Na srečo so razvijalci Jupytra že pripravili slike, ki jih lahko brez težav namestimo in poženemo z ukazom

```bash
docker run -p 8888:8888 jupyter/scipy-notebook
```

in jupyter vmesnik bo na voljo na naslovu [http://localhost:8888](http://localhost:8888). Več o [docker slikah za Jupyter](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html).

### Online storitve

Precej ponudnikov ponuja plačljiv pa tudi brezplačni dostop do jupyter zvezkov.
Tule je [seznam nekaterih](https://www.dataschool.io/cloud-services-for-jupyter-notebook/). Še najbolj enostavna je uporaba Binder-ja (s klikom na gumb zgoraj).

## Izvoz v PDF

Na voljo je več opcij. Katero boste izbrali, je odvisno od tega, kaj omogoča vaš operacijski sistem. Ko imate nameščen [jupyter](https://jupyter.org/install.html) in [XeTeX](http://xetex.sourceforge.net/), lahko izvozite vaš jupyter zvezek z ukazom

```bash
jupyter nbconvert --to pdf ImePriimek_VPISNA.ipynb
```

pri čemer seveda nadomestite `ImePriimek_VPISNA.ipynb` z imenom svoje datoteke.

### Lokalna namestitev

Za izvoz v PDF potrebujete [XeTeX](http://xetex.sourceforge.net/). Če uporabljate [linux](https://en.wikipedia.org/wiki/Linux)(samostojno ali kot [podsistem v Windowsih](https://docs.microsoft.com/en-us/windows/wsl/install-win10)) je to stvar enega ukaza. Na primer na Debian ali Ubuntu sistemih je to ukaz

```bash
sudo apt-get install texlive-xetex texlive-fonts-recommended texlive-generic-recommended
```

ki namesti ustrezne pakete. Na Windowsih priporočamo namestitev [Linux podsistema WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) in nato lahko sledite navodilom za linux.

### Docker

 Slika `jupyter/scipy-notebook` vsebuje tudi vse potrebno za izvoz zvezka v PDF.






