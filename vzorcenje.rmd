# Vzorčenje

Ta dokument lahko poženete ali pregledate v brskalniku

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/mrcinv/ovs.py/master) 
<a href="https://nbviewer.jupyter.org/github/mrcinv/ovs.py/blob/master/vzorcenje.ipynb">
    <img width="109" height="20" src="https://raw.githubusercontent.com/jupyter/design/master/logos/Badges/nbviewer_badge.png"/>
</a>

Ta dokument si lahko prenesete na svoj računalnik in poženete, tako da v terminalu napišete naslednje

    git clone https://github.com/mrcinv/ovs.py.git
    cd ovs.py
    jupyter-notebook &

# Vzorčenje slučajnih spremenljivk

Naj bo $X:\Omega\to Z_X$ slučajna spremenljivka. *Enostavni slučajni vzorec* velikosti $n$ je $n$-terica vrednosti $(x_1, x_2, \ldots x_n)$, ki jo dobimo tako, da enakomerno in neodvisno naključno izbiramo elemente iz $\Omega$ in na njih izračunamo vrednosti spremenljivke $X$. 

Enostavni slučajni vzorec si lahko predstavljamo tudi kot vrednost slučajnega vektorja $(X_1, X_2, \ldots X_n)$, kjer so spremenljivke $X_i; i=1,2,\ldots n$ enako porazdeljene in neodvisne.  

Na $\Omega$ pogosto ne moremo definirati enakomerne porazdelitve (recimo, če je $\Omega$ neomejena npr. $\mathbb{N}$ ali $\mathbb{R}$). V tem primeru uporabimo definicijo, da je enostavni slučajni vzorec vrednost slučajnega vektorja
$$(X_1,X_2, \ldots X_n).$$

## Generatorji naključnih števil
Večina programskih jezikov pozna funkcije za generiranje zaporedja naključnih števil. V Pythonu lahko uporabimo vgrajeno knjižnico [random](https://docs.python.org/2/library/random.html) za [generator pseudonaključnih števil](https://sl.wikipedia.org/wiki/Generator_psevdonaklju%C4%8Dnih_%C5%A1tevil) in [os.urandom()](https://docs.python.org/2/library/os.html#os.urandom) funkcijo, ki uporabi strojni generator naključnih števil.

Knjižnica [Scipy](https://www.scipy.org/) vsebuje tudi modul [stats](https://docs.scipy.org/doc/scipy-1.2.1/reference/tutorial/stats.html), ki implemetira najbolj pogoste porazdelitve. 
Med drugim je za vsako porazdelitev implemetiran tudi generator naključnih števil porazelejenih z dano porazdelitvijo.


```python
# generiramo 5 slučajnih vrednosti, ki so porazdeljene standardno normalno
from scipy.stats import norm
norm.rvs(size=5)
```




    array([-0.98704613, -0.77948533, -0.03270835,  0.54370647, -1.01803338])



## Naloga 1

Generiraj vzorce različnih dolžin za naslednje slučajne spremenljivke:

 1. pošten kovanec: $X$ je $0$, če pade grb in $1$, če pade cifra. ($X\sim B(1,\frac{1}{2})$)
 2. $X$ je število pik na kocki.
 3. $X$ je enakomerno porazdeljena na $[1,5]$
 4. $X\sim N(5,1)$
 
Vzorce predstavi tudi grafično (z razsevnim diagramom in histogramom).


```python
import random
import matplotlib.pyplot as plt
from scipy.stats import uniform, norm, randint, binom
```


```python
n = 30
# kovanec (0 = cifra, 1 = grb, z random)
vzorec = []
```


```python
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.hist(vzorec, bins=2, density=True, rwidth=0.8)
ax2.scatter(range(n), vzorec)
plt.show()
```

### Generator naključnih števil za poljubne porazdelitve

Knjižnica `random` implementira naključni generator le za nekatere najbolj pogoste porazdelitve. Tako so vrednosti, ki jih vrača funkcija `random.random()` porazdeljene enakomerno na $[0,1]$,
medtem ko so vrednosti, ki jih vrača funkcija `random.randint(a, b)` porazdeljene enakomerno med celimi števili, ki ležijo med `a` in `b`.

### Diskretne porazdelitve

Naj bo $U\sim E([0,1])$ enakomerno porazdeljena slučajna spremenljivka na $[0,1]$ in naj bo $X$ diskretna slučajna spremenljivka s porazdelitvijo

$$X\sim \begin{pmatrix} x_1& x_2& \ldots& x_n\cr 
                        p_1 & p_2 & \ldots & p_n
\end{pmatrix}$$

Razdelimo interval $[0,1]$ na podintervale dolžine $p_i$, tako da izberemo naraščajoče zaporedje števil $0=r_0 <r_1 < r_2< ...r_{n-1} < r_n=1$, za katerega velja
$$r_{i+1} - r_i = p_i.$$

Potem je spremenljivka $Y$, ki je definirana kot

$$
Y = \begin{cases}
        x_1;\quad U\in [0,r_1)\\
        x_2;\quad U\in [r_1,r_2)\\
        \vdots; \quad \vdots\\
        x_n;\quad U\in [r_{n-1}, 1]
    \end{cases}
$$
enako porazdeljena kot $X$.

## Naloga 2

Generiraj vzorce porazdelitev, ki nimajo vgrajenih generatorjev v Python
 1. $$X\sim \begin{pmatrix} 1& 2& 3& 4\cr 
                               0.1 & 0.2 & 0.3 & 0.4\end{pmatrix}$$
 2. $X^2+1$
 3. $X+Y^2$, kjer je $Y\sim B(10,1)$
 4. $$X\sim \begin{pmatrix} -1& 3 & 5 & 7 & 10\cr
                            0.2 & 0.1 & 0.1 & 0.2 & 0.4\end{pmatrix}.$$


```python
# pripravimo seznam, kjer je vsaka vrednost nastopa v primernem deležu
x = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4]
random.choice(x)
```




    1




```python
n = 30
vzorec1 = 
plt.hist(vzorec1, density=True, bins=4, rwidth=0.8)
vzorec2 = 
plt.hist(vzorec2, density=True, bins=4, rwidth=0.5)
```


```python
# če deleži niso racionalna števila, uporabimo enakomerno porazdelitev
def random_gen(x, p):
    """Vrne naključno generirano vrednost, ki je porazdeljena tako, da je P(X=x[i])=p[i]"""
    sum_p = 0
    u = random.random()
    for k in range(len(p)):
        sum_p += p[k]
        if u < sum_p:
            return x[k]
```


```python
n = 30
x = [-1, 3, 5, 7, 10]
p = [0.2, 0.1, 0.1, 0.2, 0.4]
vzorec1 = 
plt.hist(vzorec1, rwidth=0.8)
vzorec2 = 
plt.hist(vzorec2, rwidth=0.5)
```

### Generator naključnih števil za zvezne porazdelitve
Naj bo $U\sim E([0,1])$ enakomerno porazdeljena slučajna spremenljivka na $[0,1]$. Naj bo $X$ slučajna spremenljivka s porazdelitveno funkcijo $F_X(x)$. 
Potem je slučajna spremenljivka 

$$Y = F_X^{-1}(U)$$ 

enako porazdeljena kot $X$ (prepričaj se sam, tako da izračunaš porazdelitveno funkcijo za $Y$). 

## Naloga 3
Kaj pa če je $X$ zvezno porazdeljena? Generiraj vzorce za naslednje spremenljivke:

 1. $X\in [0, 2]$ z gostoto $p_X(x)=x/2$. 
 2. $X\in Exp(0.5)$
 


```python
n = 3000
Finv = lambda x: 2*x**0.5
vzorec1 = []
plt.hist(vzorec1, rwidth=0.8)
```

# Porazdelitve vzorčnih statistik

**Vzorčno povprečje** je slučajna spremenljivka, ki je definirana kot

$$ \bar{x} = \frac{1}{n}(X_1+X_2+\ldots +X_n).$$

**Vzorčni delež** je enak

$$\hat{p} = \frac{1}{n}(A(X_1)+A(X_2)+\ldots + A(X_n)) = \frac{k}{n},$$
kjer je $k$ število vrednosti v vzorcu, ki so v $A$ in $$A(x)=\begin{cases}1;& x\in A\\ 0; & x\not\in A\end{cases}$$

**Popravljen vzorčni standardni odklon** je enak

$$ s= \sqrt{\frac{1}{n-1}\left((X_1-\bar{x})^2+(X_2-\bar{x})^2+\ldots (X_n-\bar{x})^2\right)}$$


## Naloga 4

Za slučajno spremenljivko $X$, ki šteje število pik na kocki
 1. generiraj histogram vzorčnega povprečja
 2. generiraj histogram vzorčnega deleža za vrednosti, ki so manjše ali enake 2.
 3. generiraj histogram popravljenega vzorčnega standardnega odklona
 
za vzorce velikosti $n = 10, 100, 1000, \ldots$. 



```python
def vzorcno_povprecje(vzorec): 
    return sum(vzorec)/len(vzorec)
```


```python
N = 1000 # število vzorcev za vsako velikost
velikosti_vzorca = [10, 100, 1000] 
for n in velikosti_vzorca:
    vzorec_vzorcnih_povprecij = [vzorcno_povprecje(...) for j in range(N)]
    plt.hist(vzorec_vzorcnih_povprecij, rwidth=0.9)
```

## Naloga 5

Naloži podatke o potresih na Fidžiju `quakes` iz [podatkovnih nizov za R](https://vincentarelbundock.github.io/Rdatasets/datasets.html). Nariši histogram za magnitude in globine.

Izračunaj povprečno globino in delež potresov, globljih od 300km.

Generiraj vzorce velikosti $10, 50, 200$ in nariši histogram vzorčnega povprečja globine in vzorčnega deleža potresov globljih od 300km. Vse tri histograme za različne velikosti vzorca nariši na isto sliko.


```python
import pandas
quakes = pandas.read_csv("podatki/quakes.csv")
globine = quakes.depth
```


```python
quakes.describe()
```


```python
pi = sum(globine > 300)/len(globine)
pi
```


```python
vzorec = random.choices(globine, k=100)
```


```python
def vzorcni_delez(f, vzorec):
    "Vrne vzorčni delež elementov x, za katere je f(x) True v vzorcu"
    return sum(f(x) for x in vzorec)/len(vzorec)
```


```python

```
